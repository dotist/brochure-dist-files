(function($) {

  Drupal.behaviors.bootstrapSubthemeJs = {

    attach: function (context, settings) {

      var $body = $('body');
      var $navbar = $('#navbar');
//      var $navClone = $('.nav-clone');

      function navClonePosTest() {
        var test = $(window).scrollTop();
        var asfd;

        return $(window).scrollTop() > 0;
      }

      function navCloneScrollCtrl() {
        if (
          navClonePosTest()
          && !$body.hasClass('scrolling')
        ) {
          $body.trigger('navbarScrolling');
//          console.log('scrolling');
        } else if (
          !navClonePosTest()
          && $body.hasClass('scrolling')
        ) {
          $body.trigger('navbarParked');
//          console.log('parked');
        }
      }

      $body.on('navbarScrolling', function() {
        $(this).addClass('scrolling');
        $navbar.fadeIn();
        $('.nav-clone').fadeOut();
      });
      $body.on('navbarParked', function() {
        $body.removeClass('scrolling');
        $navbar.fadeOut();
        $('.nav-clone').fadeIn();
      });



      /*
      clone navbar, make modifications
       */
      $('#navbar', context).once(function() {
        if ($('body').hasClass('page-user')) return;

        $(this).clone().addClass('nav-clone navbar-top').insertBefore($(this));
        $(this).addClass('nav-orig');
        var src = '/sites/default/files/dbo_logo_inverse.png';
        $('.nav-clone').find('.logo img').attr('src', src);
      });

      /*
      control toggle vis of navbar elements
       */
      $('body', context).once(function() {

        // on some pages we don't want the dynamic header

        if (!$('body').is(
          '.admin-menu',
          '.page-user',
          '.page-node-edit',
          '.page-node-add'
        )) {

          // hide navbar, just cleaner that way
          $navbar.hide();

          $body.css({
            'opacity': 0
          });

          if (navClonePosTest()) {
            $(this).addClass('scrolling');
            $navbar.show();
            $('.nav-clone').hide();
          } else {
            $(this).removeClass('scrolling');
            $navbar.hide();
            $('.nav-clone').show();

          }

          $(window).scroll(function() {
            navCloneScrollCtrl();
          });


          $body.load().animate({
            'opacity': 1
          });
        }

      });

      /*
      on navbar toggle button hover, change colors of inner bars
       */
      $('.navbar-toggle', context).once(function() {
        var $button = $(this);
        var $bars = $button.children('.icon-bar');
        $button.hover(function() {
          $bars.toggleClass('hover');
        }, function() {
          $bars.toggleClass('hover');
        });
      });

      /* NAVBAR
      tag navbar for collapsed or not
      on resize, make sure that collapsed menu is converted back to normal menu
       */
      $('.navbar-collapse', context).once(function() {
        var $nav = $(this);
        var $navs = $nav.siblings('.navbar-collapse');

        $navs.each(function(){

        });

        var $button = $nav.siblings('.navbar-header').children('.navbar-toggle');

        if ($button.is(':hidden')) {
          $nav.addClass('expanded')
        } else {
          $nav.addClass('collapsed')
        };


        $(window).resize(function() {
          if (
            $button.is(':hidden')
            && $nav.hasClass('in')
            ) {
//            console.log('hidden it is')
            $nav.removeClass('in');

          }
          if ($button.is(':hidden')) {
            $nav.addClass('expanded');
            $nav.removeClass('collapsed');
          } else {
            $nav.addClass('collapsed');
            $nav.removeClass('expanded');
          }



        });
      });


      /*
      modify links in front page list-item grid - replace with #anchored
      'read more' link which is generated in the node teaser custom template
       */
      $('.node-teaser.bro-ct-el-list-item', context).once(function() {
        var $el = $(this);
        var $link = $el.find('.list-anchor-link');
        var link = $link.attr('href');
        $el.find('a').each(function(){
          if (!$(this).hasClass('.list-anchor-link')) {
            $(this).attr('href', link);
          }
        });
        $link.remove();
      });


      function zoomChildren($parent) {
        setTimeout(function(){

          function process($el, delay) {
            setTimeout(function() {
              $el.slideDown();
//              $el.animate({
//                height: 160
//              });
            }, delay);
          }

          var $elements = $parent.children();
          var delay = 200;
          $elements.each(function() {
            process($(this), delay);
            delay+=200;
          });

        },1000);
      }

      /*
      animate elements on pricing page on load
       */
      $('.pricing-page', context).once(function() {
        var $el = $(this).find('#pricing-disp-elements');
        zoomChildren($el);
      });


      /*
      dbo jobs page - hide application form when viewing backoffice jobs
       */
      $('.dschumbo-jobs', context).once(function() {
        var $form = $('.apply-form');
        var $button = $('#job-apply-link');
        $form.show();
//        $('.quicktabs-tabs li a').click(function() {

        $('.quicktabs-tabs li a').click(function() {
            var i = $(this).parent().index();
            if (i == 0 && $form.is(':hidden')) {
              $form.slideDown();
              $button.fadeIn();
            } else if (i == 1 && $form.is(':visible')) {
              $form.slideUp();
              $button.fadeOut();
            }
//          $(this).click(function() {
////            if (i == 0 && $form.is(':hidden')) {
////              $form.slideDown();
////            } else if (i == 1 && $form.is(':visible')) {
////              $form.slideUp();
////            }
////            console.log();
//          })
        });
      });

    }
  };
})(jQuery);
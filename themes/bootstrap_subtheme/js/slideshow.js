(function($) {

  Drupal.behaviors.broSlideshow = {

    attach: function (context, settings) {

//      var slideEl = 'tr'

      var $slideshow = $('.list-slideshow');
      var $pager = $('#pager');

      /* SLIDER OBJECT INIT
       *
       * @vars :
       *      $el : slideshow wrapper
       *    rowEl : row element identifier
       *   elPageCount : number of elements to display per page
       *  rowCount : number of elements per row
       *
       *  */
      function sliderEl($el, rowEl, elPageCount, rowElCount) {

        this.index = 0;
        this.rows = slideShowRows($el, rowEl);
        this.pages = slideShowPages(this.rows, elPageCount, rowElCount);

//        this.heights = getHeights(this.rows);
      }
      /* SLIDER OBJECT helper func
       * return array of row objects
       *
       * @vars :
       *      $el : slideshow wrapper
       *    rowEl : row element identifier
       *
       * */
      function slideShowRows($el, rowEl) {
        var rows = [];
        $el.find(rowEl).each(function(i) {
          var rowClass = 'slide-row-' + (i + 1);
          var $el = $('<div class="slide-row-el ' + rowClass + '"></div>');
          if ($(this).children().length) {
            $el.append($(this).children().unwrap());
//            $el.append($(this).children().unwrap()).wrap('<td class" ' + rowClass + '"></td>');
//            $el.append($(this).children().unwrap()).wrap('<td class" ' + rowClass + '"></td>');
            rows.push($el);
//            rows.push($el.parent());
//            console.log(i + ' ' + $el.find('h3').text());
          }
        });
        return rows;
      }

      /* SLIDER OBJECT helper func
       * return array of slider pages based on number of rows per page
       *
       *        @vars :
       *  elPageCount : number of elements to display per page
       *   rowElCount : number of elements per row
       *
       * */
      function slideShowPages(rows, elPageCount, rowElCount) {
        var pageCount = rows.length / elPageCount;
        pageCount = rows.length % elPageCount == 0 ? pageCount : Math.floor(pageCount) + 1;

        var $wrapper = $('<div class="slide-show-page"></table>');
        var $table = $('<table class="slide-show-page"></table>');
        var $tr = $('<tr></tr>');
        var pages = [];
        var delta = 0;



        for (var i = 0; i < pageCount; i++) {
          console.log('page = ' + i);

          var pageClass = 'slide-page-' + (i + 1);
          var $pageWrapper = $wrapper.clone().addClass(pageClass);
          var $pageTable = $table.clone().addClass(pageClass);
//          var slice = rows.slice(delta, 2);
          var slice = rows.slice(delta, elPageCount);

          $.each(slice, function(i) {
            $pageWrapper.append(this);
            console.log(i + ' ' + this.find('h3').text());
          });

          var rowCount = slice.length / rowElCount;
          rowCount = slice.length % rowElCount == 0 ? rowCount : Math.floor(rowCount)+1;

          for (var e = 0; e < rowCount; e++) {
            var rowClass = 'row-' + (e + 1);
            var $row = $tr.clone().addClass(rowClass);
            $pageTable.append($row);
//
            var rowSlice = slice.slice(0, rowElCount);
            $.each(rowSlice, function(y) {
              $row.append(rowSlice.pop());
              delta++;
            });
            delta--;

            var asdf;

//            for (var y = 0; y < rowElCount; y++) {
//              $row.append(rows[delta]);
//              delta++;
//            }

          }
//          delta += elPageCount;
          pages.unshift($pageWrapper);
//          pages.unshift($pageTable);
//          delta--;
        }

        return pages;
      }

      $pager.on('pagerInit', function() {
        var top = $slideshow.offset().top;
        var h = $slideshow.outerHeight();
        $(this).find('i').each(function() {
          $(this).css({
            'position': 'absolute',
            'top': top + (h/2)
          });
        });
        $(this).find('i').eq(0).css({ 'left': 50 });
        $(this).find('i').eq(1).css({ 'right': 50 });
      });


      $slideshow.on('slidshowInit', function() {
        // slide show, with wrapper el, element count per page, & count per row
        var slideshow = new sliderEl($slideshow, 'td', 6, 3);
        $.each(slideshow.pages, function() {
          this.insertAfter($slideshow);
        });
//        $slideshow.hide();
        var asdf;
      });

      /*
        SLIDE SHOW MAIN
       */
      $('.list-slideshow', context).once(function() {


        // init slideshow
        $slideshow.trigger('slidshowInit');
        // init pager init
        $pager.trigger('pagerInit');


      });

    }
  };
})(jQuery);
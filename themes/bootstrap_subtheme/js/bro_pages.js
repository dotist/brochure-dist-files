(function($) {

  Drupal.behaviors.bootstrapSubthemePages = {

    attach: function (context, settings) {

      /*
      BRO job page functions
       */
      /* on trigger function */
      $('.dschumbo-jobs').on('jobsPage', function() {
        var $applyLink = $('#job-apply-link button'); // button at top
        var top = $(this).offset().top; // return pos top
        var apply = $('.apply-form').offset().top; // apply pos bottom
        var $returnLink = $('#apply-return-link button'); // button at bot

        /* job page nice scrolling links - top / bottom & back */
        $applyLink.click(function() {
          $('html,body').animate({
            scrollTop: apply - 150
          }, 500);
        });
        $returnLink.click(function() {
          $('html,body').animate({
            scrollTop: top - 150
          }, 500);
        });
      });
      /* trigger the jobsPage function */
      $('.dschumbo-jobs', context).once(function() {
        $(this).trigger('jobsPage');
      }); // end job page

      $('.quicktabs-tabs a', context).once(function() {
        $(this).click(function() {
          $('.dschumbo-jobs').trigger('jobsPage');
        });
      });



    } // end attach

  }; // end Drupal behaviors

})(jQuery);
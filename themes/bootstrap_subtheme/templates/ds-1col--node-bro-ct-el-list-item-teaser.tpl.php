<?php

/**
 * @file
 * Display Suite 1 column template.
 */
$t=1;
/*
 * make anchor link to full content position in page
 */
$path = 'taxonomy/term/1';
$anchor = 'item-' . $node->nid;
//$url = 'node/7#' . $node->type . '-' . $node->nid;
$attributes = array(
  'class' => array('list-anchor-link')
);
$link = l(t('Mehr lesen'), $path, array('fragment' => $anchor, 'attributes' => $attributes));
?>
<<?php print $ds_content_wrapper; print $layout_attributes; ?> class="ds-1col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php print $ds_content; ?>
  <?php print $link ?>
</<?php print $ds_content_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>

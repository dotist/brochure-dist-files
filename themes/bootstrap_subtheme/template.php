<?php

/**
 * @file template.php
 *       bootstrap_subtheme
 */



/**
 * Implements template_preprocess_html
 * @param $variables
 */
function bootstrap_subtheme_preprocess_html(&$variables) {
  $node = menu_get_object();
  GLOBAL $language;
  $path = current_path();

  /*
   * add custom css body class based on CT
   */
  if (isset($node) && isset($node->type)) {
    $bro_class_name = '';
    switch ($node->type) {
      case 'page':
      case 'webform':
        $bro_class_name = 'bro-ct';
        break;
    }
    if (isset($bro_class_name)) {
      $variables['classes_array'][] = $bro_class_name;
    }
  }

  /*
   * add custom css body classes
   * * PRICING page
   */
  $alias = $language->name == 'German' ? 'preise' : 'pricing';
  if (
    drupal_get_path_alias($path) == $alias
  ) {
    $variables['classes_array'][] = 'pricing-page';
  }

  /*
   * add custom css body classes
   * * ENTDECKEN page
   */
  $alias = $language->name == 'German' ? 'erfahren-sie-mehr' : 'learn-more';
  if (
    drupal_get_path_alias($path) == $alias
  ) {
    $variables['classes_array'][] = 'features-page';
  }


}

/**
 * Implements template_preprocess_node()
 * @param $variables
 */
function bootstrap_subtheme_preprocess_node(&$variables) {

  if (isset($variables['node'])) {
    $node = $variables['node'];

    if (isset($node) && isset($node->type)) {

      // assign simplified node-wrapper class names based on CT EL
      $variables['classes_array'][] = str_replace('_', '-', $node->type);

    }

  }


}


///** Implements bootstrap_preprocess_page()
// * @param $variables
// */
//function bootstrap_subtheme_preprocess_page(&$variables) {
//  if (
//    isset($variables['node']->type)
//    && $variables['node']->type == 'panopoly_page'
//  ) {
//    // remove page tabs from main frame page container-node
//    unset($variables['tabs']);
//  }
//}

/*
 * Implements theme_links__locale_block
 * change output for language switcher links
 */
function bootstrap_subtheme_links__locale_block(&$vars) {
  foreach($vars['links'] as $ilanguage => $langInfo) {
    $abbr = $langInfo['language']->language;
//    $name = $langInfo['language']->name;
    $vars['links'][$ilanguage]['title'] = $abbr;
    $vars['links'][$ilanguage]['html'] = TRUE;
  }
  $content = theme_links($vars);
  return $content;
}

/**
 * Implements bootstrap_process_block
 * @param $variables
 */
function bootstrap_subtheme_process_block(&$variables) {
  // overriding block titles
//  $variables['title'] = '';
//  $variables['title'] = $variables['block']->subject;
}

/**
 * Theme function implementation for bootstrap_search_form_wrapper.
 */
function bootstrap_subtheme_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-default">';
  // We can be sure that the font icons exist in CDN.
  if (theme_get_setting('bootstrap_cdn')) {
    $output .= _bootstrap_icon('search');
  }
  else {
    $output .= '<i class="fa fa-search"></i>';
//    $output .= t('Search');
  }
  $output .= '</button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}


/**
 * Implements theme_preprocess_panels_pane
 * @param $vars
 */
function bootstrap_subtheme_preprocess_panels_pane(&$vars) {
  if ($vars['pane']->subtype == 'bro_layout-footer') {
//    $vars['content'] = '';
  }
}

/*
 * override theme functions
 */


/**
 * field override function
 *
 * field_title_display (altertative display title for references)
 * on bro_ct_el_list_item
 *
 * @param $variables
 * @return string
 */
function bootstrap_subtheme_field__field_title_display__bro_ct_el_list_item($variables) {
  $output = '';
  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    /*
     * add extra markup to the output in order to format it like a title
     */
    $path = drupal_get_path_alias('node/'
      . $variables['element']['#object']->nid, $variables['element']['#object']->language);

    $item = $variables['element']['#view_mode'] == 'teaser_2'
      ? '<h4>' . l(drupal_render($item), $path) . '</h4>' : '<h4>' . drupal_render($item) . '</h4>';

    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . $item . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * field override function
 *
 * field_icon_markup (Leistungen only)
 * on bro_ct_el_list_item in Teaser & fullview
 *
 * @param $variables
 * @return string
 */
function bootstrap_subtheme_field__field_icon_markup__bro_ct_el_list_item($variables) {
  $output = '';
  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    /*
     * add extra markup to the output in order to format it like a title
     */
    $path = drupal_get_path_alias('node/'
      . $variables['element']['#object']->nid, $variables['element']['#object']->language);

    $item = $variables['element']['#view_mode'] == 'teaser'
      ? l(drupal_render($item), $path, array('html'=> TRUE)) : drupal_render($item);

    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . $item . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}
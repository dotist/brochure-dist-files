<?php
/**
 * @file
 * bro_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bro_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bro_pages_node_info() {
  $items = array(
    'bro_ct_el_page_starter' => array(
      'name' => t('Brochure CT-EL : Page Starter'),
      'base' => 'node_content',
      'description' => t('Meant to be displayed at the top of panel pages - image with theme related coloring & short text bit.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

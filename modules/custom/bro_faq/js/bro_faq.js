(function ($) {

  Drupal.behaviors.broFaq = {
    attach: function (context, settings) {

      $('.faq', context).once(function() {
        var $faq = $(this);
        $faq.find('.faq-answer').hide();
        $faq.find('.links').hide();
      });


      $('.faq-question', context).once(function() {
        $(this).css({ cursor: 'pointer' });
        $(this).click(function() {
          var $active = $(this).next('.faq-answer');
          $active.is(':visible') ? $active.slideUp() : $active.slideDown();
          $active.siblings('.faq-answer').not($(this)).slideUp();
        });
      });

    }
  };

})(jQuery);
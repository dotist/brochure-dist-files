<?php
/**
 * @file
 * bro_webform.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function bro_webform_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'cer_settings'
  $field_bases['cer_settings'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'cer_settings',
    'foreign keys' => array(),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 1,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'cer',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
      ),
      'target_type' => 'cer',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'cer_store_settings'
  $field_bases['cer_store_settings'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'cer_store_settings',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 1,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 0,
        1 => 1,
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  return $field_bases;
}

<?php
/**
 * @file
 * bro_layout.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function bro_layout_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit_panel_context';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Node Edit Page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'css_id' => 'node-edit',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
      'sidebar' => NULL,
    ),
    'sidebar' => array(
      'style' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '56f6d0a5-d0cb-4670-8442-80de25b84871';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73569090-5f67-47b1-b6a5-634bc8104c2b';
    $pane->panel = 'contentmain';
    $pane->type = 'node_form_title';
    $pane->subtype = 'node_form_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73569090-5f67-47b1-b6a5-634bc8104c2b';
    $display->content['new-73569090-5f67-47b1-b6a5-634bc8104c2b'] = $pane;
    $display->panels['contentmain'][0] = 'new-73569090-5f67-47b1-b6a5-634bc8104c2b';
    $pane = new stdClass();
    $pane->pid = 'new-17fa67de-e033-46f2-bdd8-a138e91da0e4';
    $pane->panel = 'contentmain';
    $pane->type = 'node_form_path';
    $pane->subtype = 'node_form_path';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '17fa67de-e033-46f2-bdd8-a138e91da0e4';
    $display->content['new-17fa67de-e033-46f2-bdd8-a138e91da0e4'] = $pane;
    $display->panels['contentmain'][1] = 'new-17fa67de-e033-46f2-bdd8-a138e91da0e4';
    $pane = new stdClass();
    $pane->pid = 'new-fb4920fb-24fa-425e-b6fc-958b5ccd2d52';
    $pane->panel = 'contentmain';
    $pane->type = 'form';
    $pane->subtype = 'form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'fb4920fb-24fa-425e-b6fc-958b5ccd2d52';
    $display->content['new-fb4920fb-24fa-425e-b6fc-958b5ccd2d52'] = $pane;
    $display->panels['contentmain'][2] = 'new-fb4920fb-24fa-425e-b6fc-958b5ccd2d52';
    $pane = new stdClass();
    $pane->pid = 'new-373bd769-1e08-4ec5-85b6-0a1d2fc759fd';
    $pane->panel = 'sidebar';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_featured_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => 'Featured image',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '373bd769-1e08-4ec5-85b6-0a1d2fc759fd';
    $display->content['new-373bd769-1e08-4ec5-85b6-0a1d2fc759fd'] = $pane;
    $display->panels['sidebar'][0] = 'new-373bd769-1e08-4ec5-85b6-0a1d2fc759fd';
    $pane = new stdClass();
    $pane->pid = 'new-51126c95-cf51-4ad6-a27a-11e4260d8023';
    $pane->panel = 'sidebar';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_featured_categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 1,
      'override_title_text' => 'Content category',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '51126c95-cf51-4ad6-a27a-11e4260d8023';
    $display->content['new-51126c95-cf51-4ad6-a27a-11e4260d8023'] = $pane;
    $display->panels['sidebar'][1] = 'new-51126c95-cf51-4ad6-a27a-11e4260d8023';
    $pane = new stdClass();
    $pane->pid = 'new-99f6c0bd-36fd-411b-9833-c0be0fab7548';
    $pane->panel = 'sidebar';
    $pane->type = 'panelizer_form_default';
    $pane->subtype = 'panelizer_form_default';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '99f6c0bd-36fd-411b-9833-c0be0fab7548';
    $display->content['new-99f6c0bd-36fd-411b-9833-c0be0fab7548'] = $pane;
    $display->panels['sidebar'][2] = 'new-99f6c0bd-36fd-411b-9833-c0be0fab7548';
    $pane = new stdClass();
    $pane->pid = 'new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_menu';
    $pane->subtype = 'node_form_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'e38ab3a3-24d0-4ac2-900e-4ddb0248be0c';
    $display->content['new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c'] = $pane;
    $display->panels['sidebar'][3] = 'new-e38ab3a3-24d0-4ac2-900e-4ddb0248be0c';
    $pane = new stdClass();
    $pane->pid = 'new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_publishing';
    $pane->subtype = 'node_form_publishing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '9176b962-e64f-44a6-8ebe-e03ebf0f6c3d';
    $display->content['new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d'] = $pane;
    $display->panels['sidebar'][4] = 'new-9176b962-e64f-44a6-8ebe-e03ebf0f6c3d';
    $pane = new stdClass();
    $pane->pid = 'new-6e4a98d8-3738-41b9-8c33-4b4c905c6834';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_author';
    $pane->subtype = 'node_form_author';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '6e4a98d8-3738-41b9-8c33-4b4c905c6834';
    $display->content['new-6e4a98d8-3738-41b9-8c33-4b4c905c6834'] = $pane;
    $display->panels['sidebar'][5] = 'new-6e4a98d8-3738-41b9-8c33-4b4c905c6834';
    $pane = new stdClass();
    $pane->pid = 'new-c0926cca-059c-4bb1-bccd-2838ec7a93f2';
    $pane->panel = 'sidebar';
    $pane->type = 'node_form_buttons';
    $pane->subtype = 'node_form_buttons';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(),
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'c0926cca-059c-4bb1-bccd-2838ec7a93f2';
    $display->content['new-c0926cca-059c-4bb1-bccd-2838ec7a93f2'] = $pane;
    $display->panels['sidebar'][6] = 'new-c0926cca-059c-4bb1-bccd-2838ec7a93f2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-fb4920fb-24fa-425e-b6fc-958b5ccd2d52';
  $handler->conf['display'] = $display;
  $export['node_edit_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'List item - Reference node',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'bro-ct',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Beitrag page_starter',
        'keyword' => 'page_starter',
        'name' => 'entity:node',
        'entity_id' => '45',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'bro_ct_el_list_item' => 'bro_ct_el_list_item',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:bro_ct_el_list_item:field_term_category',
          'settings' => array(
            'field_term_category' => array(
              'und' => array(
                0 => array(
                  'tid' => '2',
                ),
              ),
            ),
            'field_term_category_tid' => array(
              0 => '2',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'radix_boxton';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '888be0a2-e5f1-4079-8282-3c72fcd1d95a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-a051c109-1527-466c-a5a2-22f3ac667b3a';
    $pane->panel = 'contentmain';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'teaser',
      'context' => 'context_entity:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a051c109-1527-466c-a5a2-22f3ac667b3a';
    $display->content['new-a051c109-1527-466c-a5a2-22f3ac667b3a'] = $pane;
    $display->panels['contentmain'][0] = 'new-a051c109-1527-466c-a5a2-22f3ac667b3a';
    $pane = new stdClass();
    $pane->pid = 'new-c2ef9061-b658-4de4-a8d1-7e2bb97cff38';
    $pane->panel = 'contentmain';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'container body-panel',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c2ef9061-b658-4de4-a8d1-7e2bb97cff38';
    $display->content['new-c2ef9061-b658-4de4-a8d1-7e2bb97cff38'] = $pane;
    $display->panels['contentmain'][1] = 'new-c2ef9061-b658-4de4-a8d1-7e2bb97cff38';
    $pane = new stdClass();
    $pane->pid = 'new-18ec4061-190a-49e7-825c-7629be68ac9c';
    $pane->panel = 'contentmain';
    $pane->type = 'block';
    $pane->subtype = 'bro_layout-footer';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'panel-footer',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '18ec4061-190a-49e7-825c-7629be68ac9c';
    $display->content['new-18ec4061-190a-49e7-825c-7629be68ac9c'] = $pane;
    $display->panels['contentmain'][2] = 'new-18ec4061-190a-49e7-825c-7629be68ac9c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}

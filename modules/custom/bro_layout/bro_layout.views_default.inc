<?php
/**
 * @file
 * bro_layout.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bro_layout_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'content_list_items';
  $view->description = 'BRO CT List items sorted by category terms';
  $view->tag = 'bro';
  $view->base_table = 'node';
  $view->human_name = 'BRO content by category lists';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'bro-list-content bro-list-teaser-grid';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Contextual filter: Content: Has taxonomy term ID (with depth, translated) */
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['id'] = 'term_node_tid_depth_i18n';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['table'] = 'node';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['field'] = 'term_node_tid_depth_i18n';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['default_action'] = 'default';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['validate']['type'] = 'i18n_taxonomy_term';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['validate_options']['type'] = 'i18n_tid';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['validate']['fail'] = 'empty';
  $handler->display->display_options['arguments']['term_node_tid_depth_i18n']['depth'] = '0';
  /* Contextual filter: Content: Has taxonomy term ID depth modifier */
  $handler->display->display_options['arguments']['term_node_tid_depth_modifier']['id'] = 'term_node_tid_depth_modifier';
  $handler->display->display_options['arguments']['term_node_tid_depth_modifier']['table'] = 'node';
  $handler->display->display_options['arguments']['term_node_tid_depth_modifier']['field'] = 'term_node_tid_depth_modifier';
  $handler->display->display_options['arguments']['term_node_tid_depth_modifier']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['term_node_tid_depth_modifier']['summary']['format'] = 'default_summary';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bro_ct_el_list_item' => 'bro_ct_el_list_item',
  );

  /* Display: Content pane Teaser - Term 1 */
  $handler = $view->new_display('panel_pane', 'Content pane Teaser - Term 1', 'panel_pane_1');
  $handler->display->display_options['display_description'] = 'bro-list items as teasers';
  $handler->display->display_options['argument_input'] = array(
    'term_node_tid_depth_i18n' => array(
      'type' => 'fixed',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '1',
      'label' => 'Content: Has taxonomy term ID (with depth, translated)',
    ),
    'term_node_tid_depth_modifier' => array(
      'type' => 'fixed',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '1',
      'label' => 'Content: Has taxonomy term ID depth modifier',
    ),
  );

  /* Display: Content pane Teaser - Term 2 */
  $handler = $view->new_display('panel_pane', 'Content pane Teaser - Term 2', 'panel_pane_3');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'bro-list-content bro-list-teaser-grid bro-list-refs';
  $handler->display->display_options['display_description'] = 'bro-list items as teasers';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser_2';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  $handler->display->display_options['argument_input'] = array(
    'term_node_tid_depth_i18n' => array(
      'type' => 'panel',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '1',
      'label' => 'Content: Has taxonomy term ID (with depth, translated)',
    ),
    'term_node_tid_depth_modifier' => array(
      'type' => 'panel',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Has taxonomy term ID depth modifier',
    ),
  );

  /* Display: Content pane LEIST */
  $handler = $view->new_display('panel_pane', 'Content pane LEIST', 'panel_pane_2');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'bro-list-content bro-list-full';
  $handler->display->display_options['display_description'] = 'bro-list items-full-content';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser_3';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['argument_input'] = array(
    'term_node_tid_depth_i18n' => array(
      'type' => 'fixed',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '1',
      'label' => 'Content: Has taxonomy term ID (with depth, translated)',
    ),
    'term_node_tid_depth_modifier' => array(
      'type' => 'fixed',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '1',
      'label' => 'Content: Has taxonomy term ID depth modifier',
    ),
  );

  /* Display: Content pane REF */
  $handler = $view->new_display('panel_pane', 'Content pane REF', 'panel_pane_4');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'bro-list-content bro-list-full';
  $handler->display->display_options['display_description'] = 'bro-list items-full-content';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['argument_input'] = array(
    'term_node_tid_depth_i18n' => array(
      'type' => 'none',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Has taxonomy term ID (with depth, translated)',
    ),
    'term_node_tid_depth_modifier' => array(
      'type' => 'none',
      'context' => 'entity:field_collection_item.archived',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Content: Has taxonomy term ID depth modifier',
    ),
  );
  $export['content_list_items'] = $view;

  return $export;
}

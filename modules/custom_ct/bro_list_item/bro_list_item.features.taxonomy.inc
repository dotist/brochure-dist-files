<?php
/**
 * @file
 * bro_list_item.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bro_list_item_taxonomy_default_vocabularies() {
  return array(
    'bro_categories' => array(
      'name' => 'Kategorien',
      'machine_name' => 'bro_categories',
      'description' => 'This is a sitewide taxonomy of categories to organize the content on your site!',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}

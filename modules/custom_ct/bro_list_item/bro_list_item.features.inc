<?php
/**
 * @file
 * bro_list_item.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bro_list_item_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bro_list_item_node_info() {
  $items = array(
    'bro_ct_el_list_item' => array(
      'name' => t('Brochure CT-EL : List Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * bro_list_item.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function bro_list_item_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|bro_ct_el_list_item|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'bro_ct_el_list_item';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'bro_pager' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|bro_ct_el_list_item|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|bro_ct_el_list_item|search_results';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'bro_ct_el_list_item';
  $ds_fieldsetting->view_mode = 'search_results';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
        'exclude node title settings' => '1',
      ),
    ),
  );
  $export['node|bro_ct_el_list_item|search_results'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|bro_ct_el_list_item|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'bro_ct_el_list_item';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'exclude node title settings' => '1',
      ),
    ),
    'node_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => 'mehr info',
        'wrapper' => 'p',
        'class' => 'more-link',
      ),
    ),
  );
  $export['node|bro_ct_el_list_item|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|bro_ct_el_list_item|teaser_2';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'bro_ct_el_list_item';
  $ds_fieldsetting->view_mode = 'teaser_2';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'exclude node title settings' => '1',
      ),
    ),
  );
  $export['node|bro_ct_el_list_item|teaser_2'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function bro_list_item_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bro_ct_el_list_item|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bro_ct_el_list_item';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'panels-radix_moscone_flipped';
  $ds_layout->settings = array(
    'regions' => array(
      'sidebar' => array(
        0 => 'field_image',
        1 => 'bro_pager',
      ),
      'contentmain' => array(
        2 => 'field_image_foto',
        3 => 'field_title_display',
        4 => 'field_list_body',
      ),
    ),
    'fields' => array(
      'field_image' => 'sidebar',
      'bro_pager' => 'sidebar',
      'field_image_foto' => 'contentmain',
      'field_title_display' => 'contentmain',
      'field_list_body' => 'contentmain',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'sidebar' => 'div',
      'contentmain' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|bro_ct_el_list_item|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bro_ct_el_list_item|search_results';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bro_ct_el_list_item';
  $ds_layout->view_mode = 'search_results';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_list_body',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_list_body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|bro_ct_el_list_item|search_results'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bro_ct_el_list_item|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bro_ct_el_list_item';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_icon_markup',
        1 => 'title',
        2 => 'field_list_body',
        3 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_icon_markup' => 'ds_content',
      'title' => 'ds_content',
      'field_list_body' => 'ds_content',
      'node_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|bro_ct_el_list_item|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bro_ct_el_list_item|teaser_2';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bro_ct_el_list_item';
  $ds_layout->view_mode = 'teaser_2';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_title_display',
        2 => 'field_list_body',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_title_display' => 'ds_content',
      'field_list_body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|bro_ct_el_list_item|teaser_2'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|bro_ct_el_list_item|teaser_3';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'bro_ct_el_list_item';
  $ds_layout->view_mode = 'teaser_3';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image_foto',
        1 => 'title',
        2 => 'field_icon_markup',
        3 => 'field_list_body',
      ),
    ),
    'fields' => array(
      'field_image_foto' => 'ds_content',
      'title' => 'ds_content',
      'field_icon_markup' => 'ds_content',
      'field_list_body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|bro_ct_el_list_item|teaser_3'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function bro_list_item_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_2';
  $ds_view_mode->label = 'Teaser 2';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['teaser_2'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_3';
  $ds_view_mode->label = 'teaser 3';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['teaser_3'] = $ds_view_mode;

  return $export;
}

<?php
/**
 * @file
 * bro_list_item.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bro_list_item_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_top|node|bro_ct_el_list_item|form';
  $field_group->group_name = 'group_top';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bro_ct_el_list_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Top',
    'weight' => '2',
    'children' => array(
      0 => 'field_term_category',
      1 => 'language',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Top',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-top field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_top|node|bro_ct_el_list_item|form'] = $field_group;

  return $export;
}

<?php
/**
 * @file
 * bro_job.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bro_job_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bro_job_node_info() {
  $items = array(
    'bro_job' => array(
      'name' => t('Brochure CT-EL : Job'),
      'base' => 'node_content',
      'description' => t('Describes Jobs, internal & external'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * bro_job.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function bro_job_taxonomy_default_vocabularies() {
  return array(
    'job_type' => array(
      'name' => 'Art von Job',
      'machine_name' => 'job_type',
      'description' => 'For categorising the CT Jobs - basically internal & external jobs (b/c this is an agency)',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
